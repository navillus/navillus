'use strict'

const { isArray, uniq } = require('..')

describe('utils/uniq', () => {
  it('returns an array', () => {
    const result = uniq([1, 2, 3])
    expect(isArray(result)).toBeTruthy()
  })

  it('returns only uniq values', () => {
    expect(uniq([1, 2, 1, 1])).toEqual([1, 2])
  })

  it('handles strings', () => {
    expect(uniq(['a', 'b', 'a', 'c'])).toEqual(['a', 'b', 'c'])
  })

  it('handles booleans', () => {
    expect(uniq([false, true, true, false])).toEqual([false, true])
  })

  it('handles objects', () => {
    const a = { x: 1 }
    const b = { y: 2 }

    expect(uniq([a, b, b, a])).toEqual([a, b])
  })
})
