'use strict'

const { pipe } = require('..')

describe('utils/pipe', () => {
  const add1 = (x) => x + 1
  const double = (x) => x * 2

  const piped = pipe(add1, double)

  it('returns a function', () => {
    expect(typeof piped).toBe('function')
  })

  it('performs left-to-right function composition', () => {
    expect(piped(2)).toBe(6)
  })
})
