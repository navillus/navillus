'use strict'

const { identity } = require('..')

describe('utils/identity', () => {
  it('returns the same value', () => {
    expect(identity(42)).toBe(42)
  })

  it('maintains referential integrity', () => {
    const obj = { a: 1, b: 2 }
    expect(identity(obj)).toBe(obj)
  })

  it('can be partially applied', () => {
    expect(identity()(42)).toBe(42)
  })
})
