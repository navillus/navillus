'use strict'

const { keys } = require('..')

describe('utils/keys', () => {
  it('returns a list of known keys', () => {
    expect(keys({ x: 1, y: 2, z: 3 })).toEqual(['x', 'y', 'z'])
  })

  it('returns an empty array for empty objects', () => {
    expect(keys({})).toEqual([])
  })

  it('returns an empty array when given undefined', () => {
    expect(keys(undefined)).toEqual([])
  })

  it('returns an empty array when given null', () => {
    expect(keys(null)).toEqual([])
  })

  it('returns an empty array when given NaN', () => {
    expect(keys(NaN)).toEqual([])
  })

  it('returns an empty array when given a string', () => {
    expect(keys('abc')).toEqual([])
  })

  it('returns an empty array when given a number', () => {
    expect(keys(42)).toEqual([])
  })

  it('returns an empty array when given a function', () => {
    expect(keys(Math.max)).toEqual([])
  })
})
