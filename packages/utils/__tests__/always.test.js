'use strict'

const { always } = require('..')

describe('utils/always', () => {
  it('returns a function that returns the object initially provided', () => {
    const life = always(42)
    expect(life()).toBe(42)
    expect(life(10)).toBe(42)
    expect(life(false)).toBe(42)
  })

  it('works with various types', () => {
    expect(always(false)()).toBe(false)
    expect(always('abc')()).toBe('abc')
    expect(always({ x: 1, y: 2 })()).toEqual({ x: 1, y: 2 })
  })
})
