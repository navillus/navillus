'use strict'

const { add } = require('..')

describe('utils/add', () => {
  it('adds two numbers', () => {
    expect(add(1, 2)).toBe(3)
  })

  it('can be partially applied', () => {
    expect(add(1)(2)).toBe(3)
  })
})
