'use strict'

const { replace } = require('..')

describe('utils/replace', () => {
  it('replaces the first matching string', () => {
    expect(replace('foo', 'bar', 'foo foo foo')).toBe('bar foo foo')
  })

  it('replaces the first match from a regex', () => {
    expect(replace(/foo/, 'bar', 'foo foo foo')).toBe('bar foo foo')
  })

  it('works with a global regex flag', () => {
    expect(replace(/foo/g, 'bar', 'foo foo foo')).toBe('bar bar bar')
  })

  it('can be partially applied', () => {
    expect(replace('foo')('bar')('foo foo foo')).toBe('bar foo foo')
  })
})
