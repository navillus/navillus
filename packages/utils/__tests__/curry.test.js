'use strict'

const { curry } = require('..')

describe('utils/curry', () => {
  const fn = (a, b, c) => a + b + c

  it('returns a function', () => {
    expect(typeof curry).toBe('function')
  })

  it('can be partially applied', () => {
    const curried = curry(fn)
    expect(curried(1)(2)(3)).toBe(6)
    expect(curried(1, 2)(3)).toBe(6)
    expect(curried(1)(2, 3)).toBe(6)
    expect(curried(1, 2, 3)).toBe(6)
  })
})
