'use strict'

const { reduce } = require('..')

describe('utils/reduce', () => {
  const add = (a, b) => a + b
  const mult = (a, b) => a * b

  it('folds simple functions over arrays', () => {
    expect(reduce(add, 0, [1, 2, 3, 4])).toBe(10)
    expect(reduce(mult, 1, [1, 2, 3, 4])).toBe(24)
  })

  it('dispatches to objects that implement `reduce`', () => {
    const obj = { x: [1, 2, 3], reduce: () => 'override' }
    expect(reduce(add, 0, obj)).toBe('override')
    expect(reduce(mult, 1, obj)).toBe('override')
  })

  it('returns the accumulator for an empty array', () => {
    expect(reduce(add, 0, [])).toBe(0)
    expect(reduce(mult, 1, [])).toBe(1)
  })

  it('can be partially applied', () => {
    expect(reduce(add)(0)([1, 2, 3, 4])).toBe(10)
    expect(reduce(add, 0)([1, 2, 3, 4])).toBe(10)
    expect(reduce(add)(0, [1, 2, 3, 4])).toBe(10)
  })
})
