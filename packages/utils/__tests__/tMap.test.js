'use strict'

const { compose, pipe, tMap } = require('..')

describe('utils/tMap', () => {
  const double = (x) => x * 2
  const add1 = (x) => x + 1

  const tDouble = tMap(double)
  const tAdd1 = tMap(add1)

  const arrayReducer = (acc, next) => acc.concat(next)

  const list = [1, 2, 3]

  it('runs the tranform on every item in the list', () => {
    expect(list.reduce(tDouble(arrayReducer), [])).toEqual([2, 4, 6])
  })

  it('can be used in combination', () => {
    expect(list.reduce(tAdd1(tDouble(arrayReducer)), [])).toEqual([4, 6, 8])
  })

  it('works with compose', () => {
    const pipeline = compose(tAdd1, tDouble)(arrayReducer)

    expect(list.reduce(pipeline, [])).toEqual([4, 6, 8])
  })

  it('works with pipe', () => {
    const pipeline = pipe(tDouble, tAdd1)(arrayReducer)

    expect(list.reduce(pipeline, [])).toEqual([4, 6, 8])
  })
})
