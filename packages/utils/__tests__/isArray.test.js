'use strict'

const { isArray } = require('..')

describe('utils/isArray', () => {
  it('returns true for an array', () => {
    expect(isArray([1, 2, 3])).toBeTruthy()
  })

  it('returns true for an empty array', () => {
    expect(isArray([])).toBeTruthy()
  })

  it('returns false for an object', () => {
    expect(isArray({})).toBeFalsy()
  })

  it('returns false for a voolean', () => {
    expect(isArray(true)).toBeFalsy()
  })

  it('returns false for a number', () => {
    expect(isArray(1)).toBeFalsy()
  })

  it('returns false for a string', () => {
    expect(isArray('abc')).toBeFalsy()
  })

  it('returns false for undefined', () => {
    expect(isArray(undefined)).toBeFalsy()
  })

  it('returns false for a null', () => {
    expect(isArray(null)).toBeFalsy()
  })

  it('returns false for NaN', () => {
    expect(isArray(NaN)).toBeFalsy()
  })

  it('returns false for a function', () => {
    expect(isArray(() => {})).toBeFalsy()
  })

  it('can be partially applied', () => {
    expect(isArray()([1, 2, 3])).toBeTruthy()
  })
})
