'use strict'

const { isObject } = require('..')

describe('utils/isObject', () => {
  it('returns true for an object', () => {
    expect(isObject({ a: 1 })).toBeTruthy()
  })

  it('returns true for an empty object', () => {
    expect(isObject({})).toBeTruthy()
  })

  it('returns false for an array', () => {
    expect(isObject([1, 2, 3])).toBeFalsy()
  })

  it('returns false for a number', () => {
    expect(isObject(1)).toBeFalsy()
  })

  it('returns false for an string', () => {
    expect(isObject('abc')).toBeFalsy()
  })

  it('returns false for a boolean', () => {
    expect(isObject(true)).toBeFalsy()
  })

  it('returns false for undefined', () => {
    expect(isObject(undefined)).toBeFalsy()
  })

  it('returns false for null', () => {
    expect(isObject(null)).toBeFalsy()
  })

  it('returns false for NaN', () => {
    expect(isObject(NaN)).toBeFalsy()
  })

  it('returns false for a function', () => {
    expect(isObject(() => {})).toBeFalsy()
  })
})
