'use strict'

const { arrayReducer } = require('..')

describe('utils/arrayReducer', () => {
  const list = [1, 2, 3]

  it('reduces by appending to the end of an array', () => {
    expect(list.reduce(arrayReducer, [])).toEqual([1, 2, 3])
  })

  it('respects items in the initial array', () => {
    expect(list.reduce(arrayReducer, [0])).toEqual([0, 1, 2, 3])
  })
})
