'use strict'

const { merge } = require('..')

describe('utils/merge', () => {
  const a = {
    x: 1,
    y: 2,
  }
  const b = {
    y: 3,
    z: 4,
  }

  it('combines two objects', () => {
    expect(merge({ x: 1 }, { y: 2 })).toEqual({ x: 1, y: 2 })
  })

  it('prioritizes properties from the second object', () => {
    expect(merge(a, b)).toEqual({ x: 1, y: 3, z: 4 })
  })

  it('can be partially applied', () => {
    expect(merge(a)(b)).toEqual({ x: 1, y: 3, z: 4 })
  })
})
