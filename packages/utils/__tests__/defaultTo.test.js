'use strict'

const { defaultTo } = require('..')

describe('utils/defaultTo', () => {
  it('defaults when given undefined', () => {
    expect(defaultTo(10, undefined)).toBe(10)
  })

  it('defaults when given null', () => {
    expect(defaultTo(10, null)).toBe(10)
  })

  it('defaults when given NaN', () => {
    expect(defaultTo(10, parseInt('abc', 10))).toBe(10)
  })

  it('does not override integers', () => {
    expect(defaultTo(10, -1)).toBe(-1)
    expect(defaultTo(10, 0)).toBe(0)
    expect(defaultTo(10, 1)).toBe(1)
    expect(defaultTo(10, 100)).toBe(100)
  })

  it('does not override strings', () => {
    expect(defaultTo('abc', '')).toBe('')
    expect(defaultTo('abc', 'a')).toBe('a')
  })

  it('does not override objects', () => {
    expect(defaultTo({ x: 1 }, {})).toEqual({})
    expect(defaultTo({ x: 1 }, { y: 2 })).toEqual({ y: 2 })
  })

  it('can be partially applied', () => {
    expect(defaultTo(1)(undefined)).toBe(1)
    expect(defaultTo(1)(10)).toBe(10)
  })
})
