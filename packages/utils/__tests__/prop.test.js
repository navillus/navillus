'use strict'

const { prop } = require('..')

describe('utils/prop', () => {
  it('handles known props', () => {
    expect(prop('a', { a: 1, b: 2 })).toBe(1)
  })

  it('returns undefined for unknown props', () => {
    expect(prop('b', { a: 1 })).toBeUndefined()
  })

  it('returns undefined for undefined objects', () => {
    expect(prop('a', undefined)).toBeUndefined()
  })

  it('returns undefined for null objects', () => {
    expect(prop('a', null)).toBeUndefined()
  })

  it('returns undefined for NaN objects', () => {
    expect(prop('a', NaN)).toBeUndefined()
  })

  it('returns undefined for arrays', () => {
    expect(prop('a', [1, 2, 3])).toBeUndefined()
  })

  it('can be partially applied', () => {
    expect(prop('a')({ a: 1 })).toBe(1)
  })
})
