'use strict'

const { find } = require('..')

describe('utils/find', () => {
  const abby = { name: 'Abby', age: 7, hair: 'blond' }
  const fred = { name: 'Fred', age: 12, hair: 'brown' }
  const rusty = { name: 'Rusty', age: 10, hair: 'brown' }
  const alois = { name: 'Alois', age: 15, disposition: 'surly' }
  const people = [abby, fred, rusty, alois]

  const propEq = (p, val) => (x) => x[p] === val

  it('finds a match in the list', () => {
    expect(find(propEq('hair', 'blond'), people)).toBe(abby)
  })

  it('finds the first known match', () => {
    expect(find(propEq('hair', 'brown'), people)).toBe(fred)
  })

  it('returns undefined if no matches found', () => {
    expect(find(propEq('hair', 'green'), people)).toBeUndefined()
  })

  it('can be partially applied', () => {
    const hasBlondHair = propEq('hair', 'blond')

    expect(find(hasBlondHair)(people)).toBe(abby)
  })
})
