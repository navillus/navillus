'use strict'

const { stringReducer } = require('..')

describe('utils/stringReducer', () => {
  const list = ['a', 'bc', 'def']

  it('reduces by appending to the end of a string', () => {
    expect(list.reduce(stringReducer, '')).toBe('abcdef')
  })

  it('respects items in the initial string', () => {
    expect(list.reduce(stringReducer, 'ghi')).toBe('ghiabcdef')
  })
})
