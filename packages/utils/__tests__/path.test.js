'use strict'

const { path } = require('..')

describe('utils/path', () => {
  it('finds a known value', () => {
    expect(path(['a', 'b'], { a: { b: 2 } })).toBe(2)
  })

  it('returns undefined for an unknown path', () => {
    expect(path(['a', 'b'], { c: { b: 2 } })).toBeUndefined()
  })

  it('handles an undefined object', () => {
    expect(path(['a', 'b'], undefined)).toBeUndefined()
  })

  it('can be partially applied', () => {
    expect(path(['a', 'b'])({ a: { b: 2 } })).toBe(2)
  })
})
