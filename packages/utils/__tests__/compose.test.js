'use strict'

const { compose } = require('..')

describe('utils/compose', () => {
  const add1 = (x) => x + 1
  const double = (x) => x * 2

  const composed = compose(add1, double)

  it('returns a function', () => {
    expect(typeof composed).toBe('function')
  })

  it('performs right-to-left function composition', () => {
    expect(composed(2)).toBe(5)
  })
})
