'use strict'

const { uniqBy } = require('..')

describe('utils/uniqBy', () => {
  it('returns only unique items', () => {
    expect(uniqBy(Math.abs, [0, 1, 2, 1])).toEqual([0, 1, 2])
  })

  it('prefers the first unique item found', () => {
    expect(uniqBy(Math.abs, [-1, 0, 1, 2])).toEqual([-1, 0, 2])
  })

  it('can be paritally applied', () => {
    expect(uniqBy(Math.abs)([-1, 0, 1, 2])).toEqual([-1, 0, 2])
  })

  it('maintains referential integrity', () => {
    const list = [{ x: -1 }, { x: 0 }, { x: 1 }, { x: 2 }]

    const fn = ({ x }) => Math.abs(x)

    const result = uniqBy(fn, list)

    expect(result[0]).toBe(list[0])
    expect(result[1]).toBe(list[1])
    expect(result[2]).toBe(list[3])
  })
})
