'use strict'

const { compose, pipe, tFilter } = require('..')

describe('utils/tFilter', () => {
  const isEven = (x) => x % 2 === 0
  const greaterThan = (x) => (y) => y > x

  const tIsEven = tFilter(isEven)
  const tGreaterThan = tFilter(greaterThan(10))

  const arrayReducer = (acc, next) => acc.concat(next)

  const list = [8, 9, 10, 11, 12, 13, 14]

  it('filters items in a reducer', () => {
    expect(list.reduce(tIsEven(arrayReducer), [])).toEqual([8, 10, 12, 14])
  })

  it('can be used in combination', () => {
    expect(list.reduce(tIsEven(tGreaterThan(arrayReducer)), [])).toEqual([
      12,
      14,
    ])
  })

  it('works with compose', () => {
    const pipeline = compose(tGreaterThan, tIsEven)(arrayReducer)

    expect(list.reduce(pipeline, [])).toEqual([12, 14])
  })

  it('works with pipe', () => {
    const pipeline = compose(tIsEven, tGreaterThan)(arrayReducer)

    expect(list.reduce(pipeline, [])).toEqual([12, 14])
  })
})
