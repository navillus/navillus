'use strict'

/**
 * Creates a transducer to map items based on the given transform
 *
 * @param {Function} transform Transform function to be run on the items
 * @returns {Function} Transducer function that transforms every item in the list
 */
function tMap(transform) {
  return (reducer) => (acc, next) => reducer(acc, transform(next))
}

module.exports = tMap
