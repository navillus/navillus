'use strict'

const curry = require('./curry')

/**
 * Returns a list containing the names of all the enumerable own properties of
 * the supplied object.
 *
 * @param {Object} obj The object to extract properties from
 * @returns {Array} An array of the object's own properties
 */
function keys(obj) {
  return Object(obj) !== obj ? [] : Object.keys(obj)
}

module.exports = curry(keys)
