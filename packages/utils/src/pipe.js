'use strict'

/**
 * Performs left-to-right function composition
 *
 * @param  {...Function} fns The functions to compose
 * @returns {Function}
 */
function pipe(...fns) {
  return fns.reduce((acc, fn) => (val) => fn(acc(val)))
}

module.exports = pipe
