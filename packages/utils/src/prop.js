'use strict'

const curry = require('./curry')
const path = require('./path')

/**
 * Returns a function that when supplied an object returns the indicated
 * property of that object, if it exists
 *
 * @param {*} p The property name
 * @param {*} obj The object to query
 * @returns {*} The value of `obj.p`
 */
function prop(p, obj) {
  return path([p], obj)
}

module.exports = curry(prop)
