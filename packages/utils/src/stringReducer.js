'use strict'

/**
 * Reducer function that appends strings.
 *
 * @param {String} acc Accumulator for the reducer
 * @param {String} next Next string to be processed
 */
function stringReducer(acc, next) {
  return acc + next
}

module.exports = stringReducer
