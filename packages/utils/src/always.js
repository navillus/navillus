'use strict'

const curry = require('./curry')

/**
 * Returns a function that always returns the given value
 *
 * @param {*} val The value to wrap in a function
 * @returns {Function} A function that always returns `val`
 */
function always(val) {
  return () => val
}

module.exports = curry(always)
