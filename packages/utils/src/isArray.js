'use strict'

const curry = require('./curry')

/**
 * Tests whether or not an object is an array
 *
 * @param {*} val The object to test
 * @returns {Boolean} `true` if `val` is an array, `false` otherwise
 */
function isArray(val) {
  return (
    val !== null &&
    val !== undefined &&
    val.length >= 0 &&
    Object.prototype.toString.call(val) === '[object Array]'
  )
}

module.exports = curry(isArray)
