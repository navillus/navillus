'use strict'

const curry = require('./curry')

/**
 * Tests whether or not an object is an object
 *
 * @param {*} val The object to test
 * @returns {Boolean} `true` if `val` is an object, `false` otherwise
 */
function isObject(val) {
  return Object.prototype.toString.call(val) === '[object Object]'
}

module.exports = curry(isObject)
