'use strict'

const curry = require('./curry')

/**
 * A function that does nothing but return the parameter supplied to it. Good
 * as a default or placeholder function.
 *
 * @param {*} x The value to return
 * @returns {*} The input value, `x`
 */
function identity(x) {
  return x
}

module.exports = curry(identity)
