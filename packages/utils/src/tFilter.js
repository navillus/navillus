'use strict'

/**
 * Creates a transducer to filter items based on the given predicate
 *
 * @param {Function} predicate Predicate used to filter items in the reducer
 * @returns {Function} Transducer function that filters items in the list
 */
function tFilter(predicate) {
  return (reducer) => (acc, next) =>
    predicate(next) ? reducer(acc, next) : acc
}

module.exports = tFilter
