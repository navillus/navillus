'use strict'

/**
 * Reducer function that appends items into an array.
 *
 * @param {Array} acc Accumulator for the reducer
 * @param {*} next Next item to be processed
 */
function arrayReducer(acc, next) {
  return acc.concat(next)
}

module.exports = arrayReducer
