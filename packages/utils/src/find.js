'use strict'

const curry = require('./curry')

/**
 * Returns the first element of the list which matches the predicate, or
 * `undefined` if no element matches.
 *
 * @param {Function} fn fn The predicate function used to determine if the element is the
 *        desired one
 * @param {Array} list The array to consider
 * @returns {Object} The element found, or `undefined`
 */
function find(fn, list) {
  return list.find(fn)
}

module.exports = curry(find)
