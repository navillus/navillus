'use strict'

/**
 * Returns a curried equivalent of the provided function
 * @function
 * @param {Function} fn The function to curry
 * @returns {Function} A new, curried function
 */
function curry(fn) {
  const helper = ({ func, args, prevArgs }) =>
    args.length + prevArgs.length >= func.length
      ? func(...prevArgs, ...args)
      : (...newArgs) =>
          helper({
            func,
            prevArgs: prevArgs.concat(args),
            args: newArgs,
          })

  return (...args) =>
    helper({
      func: fn,
      args,
      prevArgs: [],
    })
}

module.exports = curry
