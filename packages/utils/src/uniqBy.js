'use strict'

const curry = require('./curry')

/**
 * Returns a new list containing only one copy of each element in the original
 * list, based upon the value returned by applying the supplied function to
 * each list element. Prefers the first item if the supplied function produces
 * the same value on two items.
 *
 * @param {Function} fn A function used to produce a value to use during comparisons
 * @param {Array} list
 * @returns {Array} List of unique items, compared by `fn(x)`
 */
function uniqBy(fn, list) {
  const set = new Set()

  return list.reduce((acc, next) => {
    return set.has(fn(next)) ? acc : set.add(fn(next)) && acc.concat(next)
  }, [])
}

module.exports = curry(uniqBy)
