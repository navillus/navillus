'use strict'

const curry = require('./curry')

/**
 * Returns the second argument if it is not `null`, `undefined` or `NaN`,
 * otherwise the first argument is returned.
 *
 * @param {*} d The default value
 * @param {*} v `val` will be returned instead of `default` unless `val` is `null`, `undefined` or `NaN`
 * @returns {*} The second value if it is not `null`, `undefined` or `NaN`, otherwise the default value
 */
function defaultTo(d, v) {
  return v === null || v === undefined || Number.isNaN(v) ? d : v
}

module.exports = curry(defaultTo)
