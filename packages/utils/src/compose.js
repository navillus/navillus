'use strict'

/**
 * Performs right-to-left function composition
 *
 * @param  {...Function} fns The functions to compose
 * @returns {Function}
 */
function compose(...fns) {
  return fns.reduceRight((acc, fn) => (val) => fn(acc(val)))
}

module.exports = compose
