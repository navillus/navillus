'use strict'

const curry = require('./curry')
const identity = require('./identity')
const uniqBy = require('./uniqBy')

/**
 * Returns a new list containing only one copy of each element in the original
 * list, based upon the value returned by applying the supplied function to
 * each list element. Prefers the first item if the supplied function produces
 * the same value on two items.
 *
 * @param {Array} list The array to consider
 * @returns {Array} The list of unique items
 */
function uniq(list) {
  return uniqBy(identity, list)
}

module.exports = curry(uniq)
