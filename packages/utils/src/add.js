'use strict'

const curry = require('./curry')

/**
 * Adds two values
 *
 * @param {Number} a
 * @param {Number} b
 * @returns {Number}
 */
function add(a, b) {
  return a + b
}

module.exports = curry(add)
