'use strict'

const curry = require('./curry')

/**
 * Returns a single item by iterating through the list, successively calling
 * the iterator function and passing it an accumulator value and the current
 * value from the array, and then passing the result to the next call
 *
 * @param {Function} fn The iterator function
 * @param {*} acc The accumulator value
 * @param {Array} list The list of items to inumerate over
 * @returns {*} The final, accumulated value
 */
function reduce(fn, acc, list) {
  return list.reduce(fn, acc)
}

module.exports = curry(reduce)
