'use strict'

const curry = require('./curry')

/**
 * Create a new object with the own properties of the first object merged with
 * the own properties of the second object. If a key exists in both objects,
 * the value from the second object will be used.
 *
 * @param {Object} l
 * @param {Object} r
 * @returns {Object}
 */
function merge(l, r) {
  return Object.assign(l, r)
}
module.exports = curry(merge)
