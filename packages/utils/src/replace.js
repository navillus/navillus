'use strict'

const curry = require('./curry')

/**
 * Replace a substring or regex match in a string with a replacement
 *
 * @param {RegExp|String} regex A regular expression or a substring to match
 * @param {String} replacement The string to replace the matches with
 * @param {String} str The String to do the search and replacement in
 * @returns {String}
 */
function replace(regex, replacement, str) {
  return str.replace(regex, replacement)
}

module.exports = curry(replace)
