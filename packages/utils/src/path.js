'use strict'

const curry = require('./curry')

/**
 * Retrieve the value at a given path
 *
 * @param {*} paths The paths to use
 * @param {*} obj The object to retrieve the nested property from
 * @returns {*} The data at `path`
 */
function path(paths, obj) {
  let val = obj

  for (const prop of paths) {
    if (!val) {
      return
    }

    val = val[prop]
  }

  return val
}

module.exports = curry(path)
