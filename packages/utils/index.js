'use strict'

const add = require('./src/add')
const always = require('./src/always')
const arrayReducer = require('./src/arrayReducer')
const compose = require('./src/compose')
const curry = require('./src/curry')
const defaultTo = require('./src/defaultTo')
const find = require('./src/find')
const identity = require('./src/identity')
const isArray = require('./src/isArray')
const isObject = require('./src/isObject')
const keys = require('./src/keys')
const merge = require('./src/merge')
const path = require('./src/path')
const pipe = require('./src/pipe')
const prop = require('./src/prop')
const reduce = require('./src/reduce')
const replace = require('./src/replace')
const stringReducer = require('./src/stringReducer')
const tFilter = require('./src/tFilter')
const tMap = require('./src/tMap')
const uniq = require('./src/uniq')
const uniqBy = require('./src/uniqBy')

module.exports = {
  add,
  always,
  arrayReducer,
  compose,
  curry,
  defaultTo,
  find,
  identity,
  isArray,
  isObject,
  keys,
  merge,
  path,
  pipe,
  prop,
  reduce,
  replace,
  stringReducer,
  tFilter,
  tMap,
  uniq,
  uniqBy,
}
