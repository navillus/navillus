const https = require('https')

const contextCondition = 'production'
const stateCondition = 'ready'
const sitemapUrl = process.env.SITEMAP_URL

exports.handler = (event, _, callback) => {
  const { payload } = JSON.parse(event.body)
  const { state, context } = payload

  if (sitemapUrl && state === stateCondition && context === contextCondition) {
    console.log(`Sending sitemap ping to google for ${sitemapUrl}`)

    https.get(`https://google.com/ping?sitemap=${sitemapUrl}`, (res) => {
      res.on('end', () => {
        console.log('Sitemap Successfully Submitted')

        return callback(null, {
          statusCode: 200,
          body: 'Submitted Successfully',
        })
      })

      res.on('error', (err) => {
        console.log(err)
        return callback(err)
      })
    })
  } else {
    console.log('Conditions not met, not submitting')

    return callback(null, {
      statusCode: 200,
      body: `Conditions not met, not submitting`,
    })
  }
}
