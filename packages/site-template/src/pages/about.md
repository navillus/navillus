---
title: About Us
description: Navillus's core starter template powered by 11ty, TailwindCSS, and Netlify.
excerpt: Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil dolor ipsum voluptatum dicta eveniet commodi molestiae! Suscipit, magnam cumque, illum ab amet ipsum deserunt distinctio est dolore dolor ad ipsa cupiditate minus soluta, possimus accusamus.
permalink: /about/
eleventyNavigation:
  key: About
  order: 1
---

Ipsa facilis sint, beatae aperiam enim expedita voluptas dolores suscipit reiciendis similique alias, odio repellendus placeat asperiores error laboriosam eligendi accusantium in eos! Dolores labore excepturi doloremque. Rem ipsa quae tempore dignissimos doloribus recusandae quam quibusdam mollitia ex voluptas, repellat dolor et beatae sint nesciunt ad cupiditate cum debitis dolorum earum? Error adipisci optio in ea accusamus incidunt eligendi consequatur, quod impedit dolorem atque quaerat?

Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil dolor ipsum voluptatum dicta eveniet commodi molestiae! Suscipit, magnam cumque, illum ab amet ipsum deserunt distinctio est dolore dolor ad ipsa cupiditate minus soluta, possimus accusamus. Ipsa facilis sint, beatae aperiam enim expedita voluptas dolores suscipit reiciendis similique alias, odio repellendus placeat asperiores error laboriosam eligendi accusantium in eos! Dolores labore excepturi doloremque. Rem ipsa quae tempore dignissimos doloribus recusandae quam quibusdam mollitia ex voluptas, repellat dolor et beatae sint nesciunt ad cupiditate cum debitis dolorum earum? Error adipisci optio in ea accusamus incidunt eligendi consequatur, quod impedit dolorem atque quaerat?
