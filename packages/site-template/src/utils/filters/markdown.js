const MarkdownIt = require('markdown-it')
const markdown = MarkdownIt({ html: true })

module.exports = (value) => markdown.render(value)
