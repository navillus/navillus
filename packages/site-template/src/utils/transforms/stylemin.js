const { JSDOM } = require('jsdom')
const fs = require('fs')
const path = require('path')
const { PurgeCSS } = require('purgecss')
const CleanCSS = require('clean-css')
const utils = require('@navillus/utils')

const purgecss = new PurgeCSS()
const cleancss = new CleanCSS({ returnPromise: true })

function isHtml(outputPath) {
  return outputPath && path.extname(outputPath) === '.html'
}

function processStyle(html) {
  return async function (styleElem) {
    const href = utils.path(['dataset', 'href'], styleElem)

    const css = await fs.promises
      .readFile(path.join('dist', href))
      .then((buffer) => buffer.toString())

    const purged = await purgecss.purge({
      content: [
        {
          extension: 'html',
          raw: html,
        },
      ],
      css: [
        {
          raw: css,
        },
      ],
      defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
    })

    const { styles } = await cleancss.minify(purged[0].css)

    styleElem.innerHTML = styles
  }
}

module.exports = async function (content, outputPath) {
  if (!isHtml(outputPath)) {
    return content
  }

  const dom = new JSDOM(content)

  const styles = Array.from(
    dom.window.document.querySelectorAll('style[data-href]')
  )

  const processor = processStyle(content)
  await Promise.all(styles.map(processor))

  return dom.serialize()
}
