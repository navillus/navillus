const fs = require('fs')
const glob = require('glob')
const path = require('path')
const postcss = require('postcss')
const postcssImport = require('postcss-import')
const tailwind = require('tailwindcss')
const autoprefixer = require('autoprefixer')

const files = glob.sync('src/_includes/css/**/*.css')

module.exports = class {
  async data() {
    const styles = await Promise.all(
      files.map(async (file) => {
        const srcPath = path.join(process.cwd(), file)
        return {
          permalink: file.replace('src/_includes', ''),
          from: srcPath,
          css: await fs.promises
            .readFile(srcPath)
            .then((buffer) => buffer.toString()),
        }
      })
    )

    return {
      styles,
      pagination: {
        data: 'styles',
        size: 1,
        alias: 'style',
      },
      permalink: ({ style }) => style.permalink,
    }
  }

  async render({ style }) {
    const { from, css } = style

    const res = await postcss([postcssImport, tailwind, autoprefixer])
      .process(css, { from })
      .then(({ css }) => css)

    return res
  }
}
