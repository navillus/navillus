# Kamfly Website Template

## Features

- Static site generation with [Eleventy](https://11ty.dev)
- Utility-first with [Tailwind](https://tailwindcss.com)
- Production builds minify CSS, HTML, and JS
- Unused CSS removed with [PurgeCSS](http://purgecss.com)
- Assets used by `<img>` automatically optimized and made responsive
- Service worker provided in production for full PWA support
- Setup to run and deploy on [Netlify](https://netlify.com) and [Netlify Dev](https://netlify.com/products/dev) for local testing
- [Netlify CMS](https://netlifycms.org) for a full Admin panel, statically hosted on [Netlify](https://netlify.com)

## Requirements

### Netlify

For full instructions on deploying static sites on Netlify, see their [Step-by-Step Guide](https://www.netlify.com/blog/2016/10/27/a-step-by-step-guide-deploying-a-static-site-or-single-page-app/)

For full instructions on setting up a Netlify site with Netlify CMS and Netlify Identity, see their [Netlify CMS tutorial](https://www.netlifycms.org/docs/add-to-your-site/)

Make sure that **Netlify Identity** and **Git Gateway** are setup for in your Netlify site dahsboard.

## Motivations

[Eleventail](https://github.com/philhawksworth/eleventail)
[Eleventy Netlify Boilerplate](https://github.com/danurbanowicz/eleventy-netlify-boilerplate)
[Skeleventy](https://github.com/josephdyer/skeleventy)
[Eleventyone](https://github.com/philhawksworth/eleventyone)
