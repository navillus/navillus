const eleventyNavigationPlugin = require('@11ty/eleventy-navigation')
const eleventyImagePlugin = require('@navillus/eleventy-plugin-image')
const eleventyManifestPlugin = require('@navillus/eleventy-plugin-manifest')
const eleventyPWAPlugin = require('eleventy-plugin-pwa')
const eleventySEOPlugin = require('eleventy-plugin-seo')
const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const markdownItToc = require('markdown-it-table-of-contents')
const slugify = require('slugify')
const dateDisplayFilter = require('./src/utils/filters/dateDisplay')
const jsminFilter = require('./src/utils/filters/jsmin')
const markdownFilter = require('./src/utils/filters/markdown')
const htmlminTransform = require('./src/utils/transforms/htmlmin')
const styleminTransform = require('./src/utils/transforms/stylemin')

module.exports = function (eleventyConfig) {
  /**
   * Opts in to a full deep merge when combining the Data Cascade.
   *
   * @link https://www.11ty.dev/docs/data-deep-merge/#data-deep-merge
   */
  eleventyConfig.setDataDeepMerge(true)

  /**
   * Add custom watch targets
   *
   * @link https://www.11ty.dev/docs/config/#add-your-own-watch-targets
   */
  eleventyConfig.addWatchTarget('src/assets/')

  /**
   * Passthrough file copy
   *
   * @link https://www.11ty.dev/docs/copy/
   */
  eleventyConfig.addPassthroughCopy({ 'src/assets': '/' })

  /**
   * Add filters
   *
   * @link https://www.11ty.dev/docs/filters/
   */
  eleventyConfig.addFilter('dateDisplay', dateDisplayFilter)
  eleventyConfig.addFilter('jsmin', jsminFilter)
  eleventyConfig.addFilter('markdown', markdownFilter)

  /**
   * Add plugins
   *
   * @link https://www.11ty.devo/docs/plugins/
   */
  eleventyConfig.addPlugin(eleventyNavigationPlugin)
  eleventyConfig.addPlugin(eleventyImagePlugin, {
    input: 'src/assets',
    output: 'dist',
    include: ['img/**/*.@(jpg|jpeg|png)'],
  })
  eleventyConfig.addPlugin(eleventyManifestPlugin)
  eleventyConfig.addPlugin(eleventySEOPlugin, require('./src/_data/seo.json'))
  if (process.env.ELEVENTY_ENV === 'production') {
    eleventyConfig.addPlugin(eleventyPWAPlugin)
  }

  /**
   * Add Transforms
   *
   * @link https://www.11ty.dev/docs/config/#transforms
   */
  if (process.env.ELEVENTY_ENV === 'production') {
    eleventyConfig.addTransform('htmlmin', htmlminTransform)
    eleventyConfig.addTransform('stylemin', styleminTransform)
  }

  /**
   * Add Collections
   *
   * @link https://www.11ty.dev/docs/collections/
   */
  eleventyConfig.addCollection('blog', (collection) => {
    const blogs = collection.getFilteredByGlob('src/blog/**/*.md')

    for (let i = 0; i < blogs.length; i++) {
      const prevPost = blogs[i - 1]
      const nextPost = blogs[i + 1]

      blogs[i].data['prevPost'] = prevPost
      blogs[i].data['nextPost'] = nextPost
    }

    return blogs.reverse()
  })

  /**
   * Override default markdown library
   */
  function removeExtraText(s) {
    let newStr = String(s).replace(/New\ in\ v\d+\.\d+\.\d+/, '')
    newStr = newStr.replace(/⚠️/g, '')
    newStr = newStr.replace(/[?!]/g, '')
    newStr = newStr.replace(/<[^>]*>/g, '')
    return newStr
  }

  function markdownItSlugify(s) {
    return slugify(removeExtraText(s), { lower: true, remove: /[:’'`,]/g })
  }

  let md = markdownIt({
    html: true,
    breaks: true,
    linkify: true,
  })
    .use(markdownItAnchor, {
      permalink: true,
      slugify: markdownItSlugify,
      permalinkBefore: false,
      permalinkClass: 'direct-link',
      permalinkSymbol: '',
      level: [1, 2, 3, 4],
    })
    .use(markdownItToc, {
      includeLevel: [2, 3],
      slugify: markdownItSlugify,
      format: function (header) {
        return removeExtraText(header)
      },
      transformLink: function (link) {
        // remove backticks from markdown code
        return link.replace(/\%60/g, '')
      },
    })

  eleventyConfig.setLibrary('md', md)

  return {
    dir: {
      input: 'src',
      output: 'dist',
      data: '_data',
      includes: '_includes',
      layouts: '_includes/layouts',
    },
    passthroughFileCopy: true,
    templateFormats: ['html', 'njk', 'md', '11ty.js'],
    htmlTemplateEngine: 'njk',
    markdownTemplateEngine: 'njk',
  }
}
