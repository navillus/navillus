const path = require('path')
const { uniq } = require('@navillus/utils')

module.exports = function (config) {
  return async function (data) {
    const { reference, original } = data

    const srcWithSize = (src, size) => {
      const ext = path.extname(src)
      return src.replace(ext, `-${size}${ext}`)
    }

    const sizes = reference.inline
      ? []
      : uniq(
          config.sizes
            .concat([original.width])
            .filter((w) => w <= original.width)
            .sort((a, b) => a.width - b.width)
        ).map((width) => ({
          src: srcWithSize(reference.src, width),
          output: srcWithSize(reference.output, width),
          width,
        }))

    return {
      ...data,
      sizes,
    }
  }
}
